import multiprocessing
import queue
import sys
import time
import numpy as np

q = queue.Queue()


def record_audio(interprocess_queue):

    # To get around subtle issues with sounddevice and multiprocessing,
    # import sounddevice in the new process.
    # Using sounddevice with multiprocessing isn't a slam dunk.
    # https://github.com/spatialaudio/python-sounddevice/issues/147
    try:
        import sounddevice as sd
    except ModuleNotFoundError:
        print()
        print("sounddevice module isn't installed yet. Try")
        print("    python3 -m pip install sounddevice -U")
        print()
        print("If that doesn't work visit")
        print("https://e2eml.school/play_and_record_sounds.html")
        print()
        sys.exit()

    # Set up the audio stream connected to the microphone.
    # Setting device to None ensures that the default system microphone
    # will be selected.
    device = None
    # Query the input device to figure out what its sampling rate is
    # in samples per second.
    device_info = sd.query_devices(device, "input")
    samplerate = device_info["default_samplerate"]

    stream = sd.InputStream(
        device=device, channels=1,
        samplerate=samplerate, callback=audio_callback)
    stream.start()

    while True:
        data = q.get()
        interprocess_queue.put(data)


def audio_callback(indata, frames, time, status):
    """
    This is called repeatedly from a separate thread.
    It helps read in data for each audio block.
    """
    if status:
        print(status)
    # Explicit indexing with [:] ensures that a copy of the values are made,
    # rather than using a reference to the original variable, which
    # is likely to change.
    q.put(indata[:, [0]])


class SystemMicrophone:
    def __init__(
        self,
        device=None,
        n_samples=1024,
    ):
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None
        self.n_samples = n_samples
        self.audio_data = np.zeros(self.n_samples)

        self.q_proc = multiprocessing.Queue()
        collection_process = multiprocessing.Process(
            target=record_audio, args=(self.q_proc,), daemon=True)
        collection_process.start()

    def __str__(self):
        # String representation for summarization
        return("system microphone stream")

    def forward_pass(self, forward_in):
        self.forward_in = forward_in

        while True:
            try:
                data = self.q_proc.get_nowait()
            except queue.Empty:
                break
            shift = data.size
            self.audio_data = np.roll(self.audio_data, -shift)
            self.audio_data[-shift:] = data.ravel()

        self.forward_out = np.copy(self.audio_data)
        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        return self.backward_out


if __name__ == "__main__":
    # Test basic functionality of the block
    # by generating some data samples.
    test_block = SystemMicrophone(n_samples=8000)
    for _ in range(100):
        time.sleep(.1)
        print(test_block.forward_pass(None))
    print(test_block.forward_pass(None).shape)
