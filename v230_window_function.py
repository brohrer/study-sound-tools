import queue
import sys
from matplotlib.ticker import FixedLocator, FixedFormatter
from matplotlib.animation import FuncAnimation
import matplotlib.pyplot as plt
import numpy as np
from numpy.fft import fft
import scipy.signal as signal
try:
    import sounddevice as sd
except ModuleNotFoundError:
    print()
    print("sounddevice module isn't installed yet. Try")
    print("    python3 -m pip install sounddevice -U")
    print()
    print("If that doesn't work visit")
    print("https://e2eml.school/play_and_record_sounds.html")
    print()
    sys.exit()


def animate_frequency_response(
    bins_per_octave=24,
    device=None,
    high_cutoff=20000,
    interval=10,
    n_octaves=10,
    window_size=256,
):
    """
    bins_per_octave (float) is the number of frequency bins per octave
    (doubling of freqency). For reference, there are 12 notes per octave
    in the chromatic scale of Western music. More notes per octave will
    result in a more finely sliced frequency breakdown. Fewer notes
    per octave will make the frequency response look less jumpy.

    device (str or None)
    Which audio device to use for recording.
    Setting device to None tells sounddevice
    to automatically figure out which is
    the default device

    high_cutoff (float) is the highest frequency to consider in the plot.
    The upper range of the frequencies to consider.
    This needs to be less than half the sample rate.

    interval (int)
    How many milliseconds between plot updates?

    n_octaves (int) the number of frequency doublings to include in
    the analysis.

    window_size (int)
    How many samples of data should be included in running each fft?
    More samples means that it will be less jumpy, but also that
    It will respond to new sounds more slowly.
    """
    global AUDIO_DATA
    global BASELINE_DECIBELS
    BASELINE_DECIBELS = None
    global BINS_PER_OCTAVE
    BINS_PER_OCTAVE = bins_per_octave
    global HIGH_CUTOFF
    HIGH_CUTOFF = high_cutoff
    global N_OCTAVES
    N_OCTAVES = n_octaves
    global WINDOW_SIZE
    WINDOW_SIZE = window_size
    # global WINDOW_FUNCTION

    try:
        # Query the input device to figure out what its sampling rate is
        # in samples per second.
        device_info = sd.query_devices(device, "input")
        samplerate = device_info["default_samplerate"]
    except Exception as e:
        print("Couldn't get the sample rate for the recording device")
        print(e)
        raise

    if samplerate / 2 < HIGH_CUTOFF:
        print(f"The high cutoff you gave of {HIGH_CUTOFF} is higher than")
        print(f"half the sampling frequency (the Nyquist frequency).")
        print(f"Using the Nyquist frequency of {samplerate / 2} instead.")
        HIGH_CUTOFF = samplerate / 2

    # Get some fake data to initialize the plot with
    n_samples = int(WINDOW_SIZE * 2 ** (N_OCTAVES - 1))
    AUDIO_DATA = np.zeros(n_samples)
    # WINDOW_FUNCTION = signal.windows.parzen(WINDOW_SIZE)

    q = queue.Queue()

    def audio_callback(indata, frames, time, status):
        """
        This is called repeatedly from a separate thread.
        It helps read in data for each audio block.
        """
        # Fancy indexing creates a (necessary!) copy:
        q.put(indata[:, [0]])

    def update_plot(frame):
        """
        This is called by matplotlib for each plot update.

        Typically, audio callbacks happen more frequently than plot updates,
        therefore the queue tends to contain multiple blocks of audio data.
        """
        global AUDIO_DATA
        while True:
            try:
                data = q.get_nowait()
            except queue.Empty:
                break
            shift = len(data)
            AUDIO_DATA = np.roll(AUDIO_DATA, -shift)
            AUDIO_DATA[-shift:] = np.mean(data, axis=1)

        freqs, mags = time_to_freq(AUDIO_DATA)
        lines[0].set_ydata(mags)
        return lines

    def time_to_freq(time_series_data):
        """
        Convert a set of time sequence sound measurements to frequencies.
        """
        global BASELINE_DECIBELS
        global BINS_PER_OCTAVE
        global HIGH_CUTOFF
        global N_OCTAVES
        global WINDOW_FUNCTION
        global WINDOW_SIZE

        # The rate at which baseline decibels adapts
        # Downward and upward adaptation happen at different rates.
        # Downward adaptation is faster so that the baseline migrates
        # toward the lower end of the range. Upward adaptation is non-zero
        # so that anomalously low periods don't permanently depress the
        # scale.
        adaptation_rate_down = 1e-2
        adaptation_rate_up = 1e-4

        # Define the edges of the frequency bins.
        # These are evenly spaced on a log scale.
        bin_edges = 10 ** np.arange(
            np.log10(HIGH_CUTOFF / 2),
            np.log10(HIGH_CUTOFF),
            np.log10(2) / BINS_PER_OCTAVE)
        bin_centers = []
        bin_decibels = []

        downsampled_data = np.copy(time_series_data)
        effective_samplerate = samplerate
        for i_octave in range(N_OCTAVES):
            # Transform the frequencies to a log scale
            freqs = np.arange(WINDOW_SIZE) * effective_samplerate / WINDOW_SIZE
            # Convert the time series to frequencies using
            # the fast Fourier transform.
            data_to_use = downsampled_data[-WINDOW_SIZE:] # * WINDOW_FUNCTION
            energies = np.abs(fft(data_to_use))
            # Convert energies at each frequency to decibels
            decibels = 10 * np.log10(energies + 1e-6)
            # Step through each frequency bin and aggregate responses
            octave_bin_centers = []
            octave_bin_decibels = []
            for i_bin in range(int(bin_edges.size - 1)):
                octave_bin_centers.append(
                    (bin_edges[i_bin] + bin_edges[i_bin + 1]) /
                    2 ** (i_octave + 1))
                # Find all the frequencies that fall within the bin
                i_bin_freqs = np.where(np.logical_and(
                    freqs >= bin_edges[i_bin], freqs < bin_edges[i_bin + 1]))
                # Find the average decibels of all the bin's frequencies
                # if i_bin_freqs[0].size > 0:
                octave_bin_decibels.append(np.mean(decibels[i_bin_freqs]))
            bin_centers = list(octave_bin_centers) + bin_centers
            bin_decibels = list(octave_bin_decibels) + bin_decibels

            # effective_samplerate *= .5
            downsampled_size = downsampled_data.size // 2
            old_data = downsampled_data[:2 * downsampled_size]
            downsampled_data = (old_data[0::2] + old_data[1::2]) / 2

        bin_centers = np.array(bin_centers)
        bin_decibels = np.array(bin_decibels)
        if BASELINE_DECIBELS is None:
            BASELINE_DECIBELS = np.zeros(bin_decibels.shape)
        i_high = np.where(BASELINE_DECIBELS > bin_decibels)
        i_low = np.where(BASELINE_DECIBELS < bin_decibels)
        BASELINE_DECIBELS[i_high] = (
            BASELINE_DECIBELS[i_high] * (1 - adaptation_rate_down) +
            bin_decibels[i_high] * adaptation_rate_down)
        BASELINE_DECIBELS[i_low] = (
            BASELINE_DECIBELS[i_low] * (1 - adaptation_rate_up) +
            bin_decibels[i_low] * adaptation_rate_up)
        bin_decibels -= BASELINE_DECIBELS
        return bin_centers, bin_decibels

    bin_centers, bin_mags = time_to_freq(AUDIO_DATA)
    # Create and format the frequency domain plot
    fig = plt.figure()
    ax = fig.add_axes((.13, .15, .81, .79))
    lines = ax.plot(np.log10(bin_centers), bin_mags)
    ax.axis((
        np.log10(HIGH_CUTOFF / 2 ** N_OCTAVES),
        np.log10(HIGH_CUTOFF),
        -10, 30))
    ax.set_xlabel("frequency (Hz)")
    ax.set_ylabel("magnitude (dB)")
    x_major_formatter = FixedFormatter([
        "20", "50", "100",
        "200", "500", "1k",
        "2k", "5k", "10k",
        "20k"])
    x_major_locator = FixedLocator([
        1.3, 1.7, 2,
        2.3, 2.7, 3,
        3.3, 3.7, 4,
        4.3])
    x_minor_locator = FixedLocator([
        1.48, 1.6, 1.78, 1.85, 1.9, 1.95,
        2.48, 2.6, 2.78, 2.85, 2.9, 2.95,
        3.48, 3.6, 3.78, 3.85, 3.9, 3.95])
    ax.xaxis.set_major_locator(x_major_locator)
    ax.xaxis.set_minor_locator(x_minor_locator)
    ax.xaxis.set_major_formatter(x_major_formatter)
    ax.grid(which="both")

    try:
        # Set up the stream and kick off the collection
        stream = sd.InputStream(
            device=device, channels=1,
            samplerate=samplerate, callback=audio_callback)
        # This sets up the animation
        # FuncAnimation(fig, update_plot, interval=interval, blit=True)
        ani = FuncAnimation(fig, update_plot, interval=interval, blit=True)
        with stream:
            plt.show()

    except Exception as e:
        print(e)
        raise


if __name__ == "__main__":
    animate_frequency_response()
