import sounddevice as sd
import matplotlib.pyplot as plt

duration = 3
samplerate = 44100
n_samples = int(duration * samplerate)

recorded_array = sd.rec(n_samples, channels=2, blocking=True)

plt.plot(recorded_array[:, 0])

sd.play(recorded_array, blocking=True)

plt.show()
