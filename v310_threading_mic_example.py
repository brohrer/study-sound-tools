import queue
import threading
import time
import numpy as np
try:
    import sounddevice as sd
except ModuleNotFoundError:
    print()
    print("sounddevice module isn't installed yet. Try")
    print("    python3 -m pip install sounddevice -U")
    print()
    print("If that doesn't work visit")
    print("https://e2eml.school/play_and_record_sounds.html")
    print()
    sys.exit()


def audio_callback(indata, frames, time, status):
    """
    This is called repeatedly from a separate thread.
    It helps read in data for each audio block.
    """
    # Fancy indexing creates a (necessary!) copy:
    q.put(indata[:, [0]])


global q
q = queue.Queue()

# Set up the audio stream connected to the microphone
try:
    # Setting device to None ensures that the default system microphone
    # will be selected.
    device = None
    # Query the input device to figure out what its sampling rate is
    # in samples per second.
    device_info = sd.query_devices(device, "input")
    samplerate = device_info["default_samplerate"]
except Exception as e:
    print("Couldn't get the sample rate for the recording device")
    print(e)
    raise

# Set up the stream and kick off the collection
stream = sd.InputStream(
    device=device, channels=1,
    samplerate=samplerate, callback=audio_callback)
stream.start()

for i in range(10000):
    time.sleep(.01)
    print(f"pulling batch {i}")
    while True:
        try:
            next_snippet = q.get_nowait()
            print(next_snippet.shape)
        except queue.Empty:
            break
