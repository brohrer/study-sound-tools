import sounddevice as sd
import numpy as np
from numpy.fft import fft
import matplotlib.pyplot as plt

duration = 1
samplerate = 44100
lead_chop = 30
n_samples = int(duration * samplerate)

recorded_array_unchopped = sd.rec(
    n_samples + lead_chop, channels=2, blocking=True)
recorded_array = recorded_array_unchopped[lead_chop:, :]

mags = np.abs(fft(recorded_array[:, 0]))
time = np.arange(n_samples) / samplerate
freqs = np.arange(n_samples) / duration

# Cut in half
mags = mags[:int(n_samples / 2)]
freqs = freqs[:int(n_samples / 2)]
low_cutoff = 20
i_keep = np.where(freqs > low_cutoff)
mags = mags[i_keep]
freqs = freqs[i_keep]

fig = plt.figure()
ax_time = fig.add_axes((.1, .6, .8, .3))
ax_freq = fig.add_axes((.1, .1, .8, .3))
ax_time.plot(time, recorded_array[:, 0])
ax_freq.plot(np.log10(freqs), np.log2(mags + 1e-6))
ax_time.set_xlabel("time (s)")
ax_freq.set_xlabel("frequency (Hz)")
ax_time.grid()
ax_freq.grid()

sd.play(recorded_array, blocking=True)

plt.show()
