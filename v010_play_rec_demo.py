import sounddevice as sd

duration = 3
samplerate = 44100
n_samples = int(duration * samplerate)

recorded_array = sd.rec(n_samples, channels=2, blocking=True)

sd.play(recorded_array, blocking=True)
