import sounddevice as sd
import numpy as np
from numpy.fft import fft, fftfreq
import matplotlib.pyplot as plt

duration = 1
samplerate = 44100
lead_chop = 30
n_samples = int(duration * samplerate)

recorded_array_unchopped = sd.rec(
    n_samples + lead_chop, channels=2, blocking=True)
recorded_array = recorded_array_unchopped[lead_chop:, :]

mags = np.abs(fft(recorded_array[:, 0]))
time = np.arange(n_samples) / samplerate
freqs = np.arange(n_samples) / duration

fig = plt.figure()
ax_time = fig.add_axes((.1, .6, .8, .3))
ax_freq = fig.add_axes((.1, .1, .8, .3))
ax_time.plot(time, recorded_array[:, 0])
ax_freq.plot(freqs, mags)
ax_time.set_xlabel("time (s)")
ax_freq.set_xlabel("frequency (Hz)")

sd.play(recorded_array, blocking=True)

plt.show()
