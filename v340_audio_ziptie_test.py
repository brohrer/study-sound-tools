import matplotlib.pyplot as plt
from cottonwood.experimental.heartbeat import Heartbeat
from cottonwood.experimental.ziptie_block import Ziptie
from cottonwood.structure import Structure
# from v360_seg_fault_debug import SystemMicrophone as Microphone
# from v350_microphone_block_mock import SystemMicrophone as Microphone
# from v290_system_microphone_block import SystemMicrophone as Microphone
from v380_system_mic_multiproc_block import SystemMicrophone as Microphone
from v320_featurizer_block import TimeSeriesFrequencyFeaturizer as Featurizer


pipeline = Structure()

pipeline.add(Microphone(n_samples=int(2**16)), "microphone")
pipeline.add(Featurizer(bins_per_octave=6), "featurizer")
pipeline.add(Heartbeat(interval_ms=100), "heartbeat")
pipeline.add(Ziptie(), "ziptie")
pipeline.connect_sequence([
    "microphone",
    "featurizer",
    "heartbeat",
    "ziptie",
])

for i in range(10000):
    pipeline.forward_pass()
    features = pipeline.blocks["featurizer"].forward_out
    if i == 0:
        plt.ion()
        plt.show()
        fig = plt.figure()
        ax = fig.gca()
        ax.set_ylim(-2, 2)
        line, = ax.plot(features)
    else:
        plt.pause(0.001)
        line.set_ydata(features)
