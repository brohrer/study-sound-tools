import queue
import sys
import time
import numpy as np
try:
    import sounddevice as sd
except ModuleNotFoundError:
    print()
    print("sounddevice module isn't installed yet. Try")
    print("    python3 -m pip install sounddevice -U")
    print()
    print("If that doesn't work visit")
    print("https://e2eml.school/play_and_record_sounds.html")
    print()
    sys.exit()


def test_heartbeat():
    device = None
    try:
        # Query the input device to figure out what its sampling rate is
        # in samples per second.
        device_info = sd.query_devices(device, "input")
        samplerate = device_info["default_samplerate"]
    except Exception as e:
        print("Couldn't get the sample rate for the recording device")
        print(e)
        raise

    q = queue.Queue()

    def audio_callback(indata, frames, time, status):
        """
        This is called repeatedly from a separate thread.
        It helps read in data for each audio block.
        """
        # Fancy indexing creates a (necessary!) copy:
        q.put(indata[:, [0]])

    def read_data():
        """
        This is called by matplotlib for each plot update.

        Typically, audio callbacks happen more frequently than plot updates,
        therefore the queue tends to contain multiple blocks of audio data.
        """
        total = 0
        while True:
            try:
                data = q.get_nowait()
                print("while loop", data.shape)
            except queue.Empty:
                print("jumping while")
                break

    try:
        # Set up the stream and kick off the collection
        stream = sd.InputStream(
            device=device, channels=1,
            samplerate=samplerate, callback=audio_callback)
        with stream:
            last_time = time.time()
            duration = .0398
            while True:
                remaining = duration - (time.time() - last_time)
                if remaining < 0:
                    print("no time remaining")
                    remaining = 0
                print(f"remaining {remaining}")
                time.sleep(remaining)
                new_time = time.time()
                print(f"interval {new_time - last_time}")
                last_time = new_time

                read_data()
    except Exception as e:
        print(e)
        raise


if __name__ == "__main__":
    test_heartbeat()
