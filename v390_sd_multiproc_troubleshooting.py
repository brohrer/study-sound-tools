"""
Using sounddevice with multiprocessing isn't a slam dunk.
https://github.com/spatialaudio/python-sounddevice/issues/147
"""
import multiprocessing
import queue
import time
import numpy as np

q = queue.Queue()


def record_audio(shared_array, n_samples):

    import sounddevice as sd
    # Set up the audio stream connected to the microphone.
    # Setting device to None ensures that the default system microphone
    # will be selected.
    device = None
    # Query the input device to figure out what its sampling rate is
    # in samples per second.
    device_info = sd.query_devices(device, "input")
    samplerate = device_info["default_samplerate"]

    stream = sd.InputStream(
        device=device, channels=1,
        samplerate=samplerate, callback=audio_callback)
    stream.start()

    snippet = np.zeros(n_samples)
    done = False
    data = None
    while True:
        while not done:
            try:
                data = q.get_nowait()
            except queue.Empty:
                done = True
        if data is not None:
            shift = data.size
            snippet = np.roll(snippet, -shift)
            snippet[-shift:] = data.ravel()
            for i in range(n_samples):
                shared_array[i] = snippet[i]
        done = False


def audio_callback(indata, frames, time, status):
    """
    This is called repeatedly from a separate thread.
    It helps read in data for each audio block.
    """
    if status:
        print(status)
    # Explicit indexing with [:] ensures that a copy of the values are made,
    # rather than using a reference to the original variable, which
    # is likely to change.
    q.put(indata[:, 0])


n_samples = 1000
audio_data = multiprocessing.Array('d', n_samples)
collection_process = multiprocessing.Process(
    target=record_audio, args=(audio_data, n_samples), daemon=True)
collection_process.start()


for _ in range(10):
    time.sleep(.1)
    print(audio_data[0], audio_data[1], audio_data[2])
    print(len(audio_data))
