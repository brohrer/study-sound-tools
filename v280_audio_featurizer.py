import queue
import sys
import time
import numpy as np
from numpy.fft import fft
try:
    import sounddevice as sd
except ModuleNotFoundError:
    print()
    print("sounddevice module isn't installed yet. Try")
    print("    python3 -m pip install sounddevice -U")
    print()
    print("If that doesn't work visit")
    print("https://e2eml.school/play_and_record_sounds.html")
    print()
    sys.exit()


def featurizer_demo(
    bins_per_octave=12,
    device=None,
    freq_update_rate=.1,
    high_cutoff=20000,
    interval=40,
    n_octaves=10,
    window_size=256,
):
    """
    bins_per_octave (float) is the number of frequency bins per octave
    (doubling of freqency). For reference, there are 12 notes per octave
    in the chromatic scale of Western music. More notes per octave will
    result in a more finely sliced frequency breakdown. Fewer notes
    per octave will make the frequency response look less jumpy.

    device (str or None)
    Which audio device to use for recording.
    Setting device to None tells sounddevice
    to automatically figure out which is
    the default device

    freq_update_rate (float) controls how quickly changes in the calculated
    frequency response are reflected in the running estimate.
    Rates close to zero mean that new changes will take some time
    to show up, but the resulting frequency curve will vary smoothly.
    Rates closer to one mean that new changes will be reflected more
    quickly, but will also result in a jumpier frequency response.

    high_cutoff (float) is the highest frequency to consider in the plot.
    The upper range of the frequencies to consider.
    This needs to be less than half the sample rate.

    interval (int)
    How many milliseconds between feature updates?

    n_octaves (int) the number of frequency doublings to include in
    the analysis.

    window_size (int)
    How many samples of data should be included in running each fft?
    More samples means that it will be less jumpy, but also that
    It will respond to new sounds more slowly.
    """
    global AUDIO
    AUDIO_FREQ = None
    BINS_PER_OCTAVE = bins_per_octave
    FREQ_UPDATE_RATE = freq_update_rate
    HIGH_CUTOFF = high_cutoff
    N_OCTAVES = n_octaves
    WINDOW_SIZE = window_size

    try:
        # Query the input device to figure out what its sampling rate is
        # in samples per second.
        device_info = sd.query_devices(device, "input")
        samplerate = device_info["default_samplerate"]
    except Exception as e:
        print("Couldn't get the sample rate for the recording device")
        print(e)
        raise

    if samplerate / 2 < HIGH_CUTOFF:
        print(f"The high cutoff you gave of {HIGH_CUTOFF} is higher than")
        print(f"half the sampling frequency (the Nyquist frequency).")
        print(f"Using the Nyquist frequency of {samplerate / 2} instead.")
        HIGH_CUTOFF = samplerate / 2

    # Get some fake data to initialize the plot with
    n_samples = int(WINDOW_SIZE * 2 ** (N_OCTAVES - 1))
    AUDIO_TIME = np.zeros(n_samples)

    q = queue.Queue()

    def audio_callback(indata, frames, time, status):
        """
        This is called repeatedly from a separate thread.
        It helps read in data for each audio block.
        """
        # Fancy indexing creates a (necessary!) copy:
        q.put(indata[:, [0]])

    def get_data():
        nonlocal AUDIO_TIME
        while True:
            try:
                data = q.get_nowait()
            except queue.Empty:
                break
            shift = len(data)
            AUDIO_TIME = np.roll(AUDIO_TIME, -shift)
            AUDIO_TIME[-shift:] = np.mean(data, axis=1)

    def update_features():
        """
        Convert a set of time sequence sound measurements to frequencies.
        """
        nonlocal AUDIO_FREQ
        nonlocal AUDIO_TIME
        # global BINS_PER_OCTAVE
        # global FREQ_UPDATE_RATE
        # global HIGH_CUTOFF
        # global N_OCTAVES
        # global WINDOW_SIZE

        # Define the edges of the frequency bins.
        # These are evenly spaced on a log scale.
        bin_edges = 10 ** np.arange(
            np.log10(HIGH_CUTOFF / 2),
            np.log10(HIGH_CUTOFF),
            np.log10(2) / BINS_PER_OCTAVE)
        bin_centers = []
        bin_decibels = []

        downsampled_data = np.copy(AUDIO_TIME)
        effective_samplerate = samplerate
        for i_octave in range(N_OCTAVES):
            # Transform the frequencies to a log scale
            freqs = np.arange(WINDOW_SIZE) * effective_samplerate / WINDOW_SIZE
            # Convert the time series to frequencies using
            # the fast Fourier transform.
            data_to_use = downsampled_data[-WINDOW_SIZE:]
            energies = np.abs(fft(data_to_use))
            # Convert energies at each frequency to decibels
            decibels = 10 * np.log10(energies + 1e-6)
            # Step through each frequency bin and aggregate responses
            octave_bin_centers = []
            octave_bin_decibels = []
            for i_bin in range(int(bin_edges.size - 1)):
                octave_bin_centers.append(
                    (bin_edges[i_bin] + bin_edges[i_bin + 1]) /
                    2 ** (i_octave + 1))
                # Find all the frequencies that fall within the bin
                i_bin_freqs = np.where(np.logical_and(
                    freqs >= bin_edges[i_bin], freqs < bin_edges[i_bin + 1]))
                # Find the average decibels of all the bin's frequencies
                octave_bin_decibels.append(np.mean(decibels[i_bin_freqs]))
            bin_centers = list(octave_bin_centers) + bin_centers
            bin_decibels = list(octave_bin_decibels) + bin_decibels

            downsampled_size = downsampled_data.size // 2
            old_data = downsampled_data[:2 * downsampled_size]
            downsampled_data = (old_data[0::2] + old_data[1::2]) / 2

        if AUDIO_FREQ is None:
            AUDIO_FREQ = np.zeros(len(bin_centers))
        AUDIO_FREQ = (
            AUDIO_FREQ * (1 - FREQ_UPDATE_RATE) +
            np.array(bin_decibels) * FREQ_UPDATE_RATE)

    try:
        # Set up the stream and kick off the collection
        stream = sd.InputStream(
            device=device, channels=1,
            samplerate=samplerate, callback=audio_callback)
        with stream:
            last_timestamp = time.time()
            while True:
                remaining = interval / 1000 - (time.time() - last_timestamp)
                print(remaining)
                if remaining < 0:
                    print("no time remaining")
                    remaining = 0
                time.sleep(remaining)
                last_timestamp = time.time()

                get_data()
                update_features()
                print(AUDIO_FREQ)

    except Exception as e:
        print(e)
        raise


if __name__ == "__main__":
    featurizer_demo()
