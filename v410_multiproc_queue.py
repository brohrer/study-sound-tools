import multiprocessing
import queue
import time
import numpy as np

q_thread = queue.Queue()


def record_audio(interprocess_queue):

    # To get around subtle issues with sounddevice and multiprocessing,
    # import sounddevice in the new process.
    # Using sounddevice with multiprocessing isn't a slam dunk.
    # https://github.com/spatialaudio/python-sounddevice/issues/147
    import sounddevice as sd
    # Set up the audio stream connected to the microphone.
    # Setting device to None ensures that the default system microphone
    # will be selected.
    device = None
    # Query the input device to figure out what its sampling rate is
    # in samples per second.
    device_info = sd.query_devices(device, "input")
    samplerate = device_info["default_samplerate"]

    stream = sd.InputStream(
        device=device, channels=1,
        samplerate=samplerate, callback=audio_callback)
    stream.start()

    while True:
        data = q_thread.get()
        interprocess_queue.put(data)


def audio_callback(indata, frames, time, status):
    """
    This is called repeatedly from a separate thread.
    It helps read in data for each audio block.
    """
    if status:
        print(status)
    # Explicit indexing with [:] ensures that a copy of the values are made,
    # rather than using a reference to the original variable, which
    # is likely to change.
    q_thread.put(indata[:, 0])


q_proc = multiprocessing.Queue()
collection_process = multiprocessing.Process(
    target=record_audio, args=(q_proc,), daemon=True)
collection_process.start()

for i in range(10000):
    time.sleep(.01)
    print(f"pulling batch {i}")
    while True:
        try:
            next_snippet = q_proc.get_nowait()
            print(next_snippet)
            print(next_snippet.shape)
        except queue.Empty:
            break
